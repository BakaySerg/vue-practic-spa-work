import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import BuyModalComponent from '@/components/Shared/BuyModal';
import './plugins/vuetify'
import * as fb from 'firebase'
// import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: '#00796B',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})
Vue.component('app-buy-modal', BuyModalComponent)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  render: h => h(App),
  created() {
    fb.initializeApp({
      apiKey: 'AIzaSyD904lfYFLww0092CkSOswkBHe9d_uc_6U',
      authDomain: 'itc-ads-b5ae3.firebaseapp.com',
      databaseURL: 'https://itc-ads-b5ae3.firebaseio.com',
      projectId: 'itc-ads-b5ae3',
      storageBucket: 'itc-ads-b5ae3.appspot.com',
      messagingSenderId: '160060746493'
    })

    fb.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('autoLoginUser', user)
      }
    })

    this.$store.dispatch('fetchAds')
  }
})
